package com.neoexpert.nreader;

import android.content.*;
import android.content.pm.*;
import android.graphics.drawable.*;
import android.os.*;
import android.support.v7.app.*;
import android.view.*;
import android.widget.*;
import android.widget.CompoundButton.*;
import com.neoexpert.nreader.*;
import java.util.*;
import android.widget.AdapterView.*;

public class PackageSelector extends AppCompatActivity
implements
OnItemClickListener
{

	@Override
	public void onItemClick(AdapterView<?> p1, View p2, int pos, long p4)
	{
		Intent data=new Intent();
		data.putExtra("package",packages.get(pos).packageName);
		setResult(1,data);
		finish();
	}
	
	List<ApplicationInfo> packages;
	ListAdapter la;
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO: Implement this method
		super.onCreate(savedInstanceState);
		setContentView(R.layout.package_selector);
		final PackageManager pm = getPackageManager();
//get a list of installed apps.
		packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);

//		for (ApplicationInfo packageInfo : packages) {
//			Log.d(TAG, "Installed package :" + packageInfo.packageName);
//			Log.d(TAG, "Source dir : " + packageInfo.sourceDir);
//			Log.d(TAG, "Launch Activity :" + pm.getLaunchIntentForPackage(packageInfo.packageName)); 
//		}
// the getLaunchIntentForPackage returns an intent that you can use with startActivity() 
		la = new ListAdapter(this, R.layout.package_selector_row, packages);
		ListView lv=(ListView)findViewById(R.id.pckg_selector_list);
		lv.setAdapter(la);
		lv.setOnItemClickListener(this);
	}
	public class ListAdapter extends ArrayAdapter<ApplicationInfo>
	{

		public ListAdapter(Context context, int textViewResourceId)
		{
			super(context, textViewResourceId);
		}

		public ListAdapter(Context context, int resource, List<ApplicationInfo> items)
		{
			super(context, resource, items);
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{

			View v = convertView;

			if (v == null)
			{
				LayoutInflater vi;
				vi = LayoutInflater.from(getContext());
				v = vi.inflate(R.layout.package_selector_row, null);
			}

			ApplicationInfo p = getItem(position);

			if (p != null)
			{
				TextView tt1 = (TextView) v.findViewById(R.id.listrowTV);
				ImageView iv=(ImageView)v.findViewById(R.id.listrowIV);



				tt1.setText(p.packageName);

				Drawable icon = getPackageManager().getApplicationIcon(getItem(position));
				iv.setImageDrawable(icon);
				


			}

			return v;
		}

	}

}
