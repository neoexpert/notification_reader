package com.neoexpert.nreader;
import android.app.*;

public class App extends Application
{

	@Override
	public void onCreate()
	{
		if (BuildConfig.DEBUG)
			Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
		super.onCreate();
	}
	
}
