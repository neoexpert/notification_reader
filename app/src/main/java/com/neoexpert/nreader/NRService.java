package com.neoexpert.nreader;

import android.app.*;
import android.content.*;
import android.database.*;
import android.hardware.*;
import android.hardware.display.*;
import android.media.*;
import android.os.*;
import android.preference.*;
import android.service.notification.*;
import android.speech.tts.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import java.util.*;
import java.util.Map.*;

public class NRService extends NotificationListenerService
implements
SensorEventListener
{

	UtteranceProgressListener ups=new UtteranceProgressListener(){

		@Override
		public void onStart(String p1)
		{
			// TODO: Implement this method
		}

		@Override
		public void onDone(String key)
		{
			cancelNotification(notifs.remove(key));
		}

		@Override
		public void onError(String p1)
		{
			// TODO: Implement this method
		}
	};
	LinkedHashMap<String,String> notifs=new LinkedHashMap<String,String>(){

		@Override
		protected boolean removeEldestEntry(Map.Entry eldest)
		{
			return size() > 16;
		}
		
	};
	
	private boolean isCyrilic(String s){
		for(int i = 0; i < s.length(); i++) {
			if(Character.UnicodeBlock.of(s.charAt(i)).equals(Character.UnicodeBlock.CYRILLIC)) {
				return true;
			}
		}
		return false;
	}
	public void sound(final int n)
	{


		//if (isPlaying) return;
		//isPlaying = true;
		Runnable runnable = new Runnable() {
			@Override
			public void run()
			{


				//N++;
				//for(int i=40;i<70;i++)

				AudioTrack track=new AudioTrack(AudioManager.STREAM_MUSIC, 44100, AudioFormat.CHANNEL_CONFIGURATION_MONO, AudioFormat.ENCODING_PCM_16BIT, 1024, AudioTrack.MODE_STREAM);
				try
				{
					track.setVolume(track.getMaxVolume());
				}
				catch (NoSuchMethodError e)
				{}
				//short[] buffer = new short[44100];


				//float samples[] = new float[44100];
				while (true)
					try
					{
						track.play();
						break;
					}
					catch (Exception e)
					{continue;}

				//at[currenttrack].stop();
				//at[currenttrack].flush();
				//at[currenttrack].play();
				//

				short[] buffer = new short[441000];


				float samples[] = new float[441000];



				for (int i = 0; i < samples.length; i++)
				{

					samples[i] = (float)  Math.sin((float)i * ((float)(2 * Math.PI) *  getNoteF(n) / 44100)) * (float)Math.exp(-i * 0.0003);    //the part that makes this a sine wave....

					buffer[i] = (short) (samples[i] * Short.MAX_VALUE);
				}
				track.write(buffer, 0, buffer.length);  //write to the audio buffer.... and start all over again!
				try
				{
					Thread.sleep(2000);
				}
				catch (InterruptedException e)
				{}
				track.stop();

				track.release();
				//boolean done=false;



				//isPlaying = false;
			}           


		};
		new Thread(runnable).start();

	}
	public double getNoteF(int n)
	{
		return Math.pow(2, ((double)n - 49.0) / 12.0) * 440.0;
	}
	boolean mutetts=false;
	@Override
	public void onSensorChanged(SensorEvent e)
	{
		
		if(e.sensor.getType() == Sensor.TYPE_PROXIMITY)
		{
			
			//sound(75);
			if(e.values[0]==0.0f){
				tts.stop();
				mutetts=true;
			}
			else 
			{
				//tts.stop();
				mutetts=false;
			}
			//Toast.makeText(getApplicationContext(), e.values[0]+"", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onAccuracyChanged(Sensor p1, int p2)
	{
		// TODO: Implement this method
	}
	

	
	

	SharedPreferences prefs;
	SharedPreferences.Editor prefsEditor;
    private String TAG = this.getClass().getSimpleName();
    private NLServiceReceiver nlservicereciver;
	TextToSpeech tts;
	public static boolean created=false;
	SensorManager mSensorManager;
	Sensor mProximity;
    @Override
    public void onCreate()
	{
        super.onCreate();
		created=true;
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mProximity = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
		mSensorManager.registerListener(this, mProximity, SensorManager.SENSOR_DELAY_NORMAL);
		
		
		SettingsContentObserver mSettingsContentObserver = new SettingsContentObserver(this,new Handler());
		getApplicationContext().getContentResolver().registerContentObserver(android.provider.Settings.System.CONTENT_URI, true, mSettingsContentObserver );
		prefs = PreferenceManager.getDefaultSharedPreferences(this);
		prefsEditor = prefs.edit();
        nlservicereciver = new NLServiceReceiver();
        //IntentFilter filter = new IntentFilter();
        //filter.addAction("com.neoexpert.nreader.NOTIFICATION_LISTENER_SERVICE");
        //registerReceiver(nlservicereciver, filter);
		tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
				@Override
				public void onInit(int status)
				{
					if(status==TextToSpeech.SUCCESS)
						tts.setOnUtteranceProgressListener(ups);

				}});
		//tts.setSpeechRate(0.8f);
		//tts.speak("test", TextToSpeech.QUEUE_ADD, null);
		packgs=prefs.getStringSet("packages",packgs);
		
		
	}

    @Override
    public void onDestroy()
	{
        super.onDestroy();
		created=false;
		Intent i = new  Intent("com.neoexpert.nreader.NOTIFICATION_LISTENER_SERVICE");
		i.putExtra("action", 1);
		sendBroadcast(i);
        //unregisterReceiver(nlservicereciver);
    }
	Set<String> packgs=new HashSet<String>();
	
    @Override
    public void onNotificationPosted(StatusBarNotification sbn)
	{
		if(mutetts) return;
		if(!prefs.getBoolean("ServiceEnabled", false)) return;
		if(!prefs.getBoolean("readWhenScreenON",false))
			if(isScreenOn())return;
		String p=sbn.getPackageName();
		if (!packgs.contains(p)) {
			Intent i = new  Intent("com.neoexpert.nreader.NOTIFICATION_LISTENER_SERVICE");
			i.putExtra("action", 0);
			i.putExtra("package", p);
			sendBroadcast(i);
			packgs.add(p);
			prefsEditor.putStringSet("packages",packgs);
			prefsEditor.commit();
		}
		
		if(!prefs.getBoolean(p,false))return;
        //Log.i(TAG, "**********  onNotificationPosted");
        //Log.i(TAG, "ID :" + sbn.getId() + "\t" + sbn.getNotification().tickerText + "\t" + sbn.getPackageName());
        //Intent i = new  Intent("com.neoexpert.nreader.NOTIFICATION_LISTENER_SERVICE");
        //i.putExtra("notification_event", "onNotificationPosted :" + sbn.getPackageName() + "\n");
        //sendBroadcast(i);
		
		Notification n = null;
		
			n = sbn.getNotification();
		if(n.extras==null)return;
		CharSequence textcharsequenze =
	
			textcharsequenze = n.extras
			.getCharSequence(Notification.EXTRA_TEXT);
		String text = null;
		if (textcharsequenze != null)
			text = textcharsequenze.toString();
		
		else return;
		if (text.contains("WhatsApp") && (text.contains("Web") || text.contains("Video")))
			return;
		

		String title=sbn.getNotification().extras.getCharSequence(Notification.EXTRA_TITLE).toString();
		String URLregex = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
		text=text.replaceAll(URLregex, "URL");
		
			
		text=title + ": " + text;
		if(notifs.containsKey(text)) return;
		
		notifs.put(text,sbn.getKey());
		if(isCyrilic(text)){
			Locale l=tts.getLanguage();
			tts.setLanguage( new Locale("ru", "RU"));
			tts.speak(text, TextToSpeech.QUEUE_ADD, null,text);
			tts.setLanguage(l);
		}else
			tts.speak(text, TextToSpeech.QUEUE_ADD, null,text);
	
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn)
	{
		tts.stop();
        Log.i(TAG, "********** onNOtificationRemoved");
        Log.i(TAG, "ID :" + sbn.getId() + "\t" + sbn.getNotification().tickerText + "\t" + sbn.getPackageName());
       // Intent i = new  Intent("com.neoexpert.nreader.NOTIFICATION_LISTENER_SERVICE");
       // i.putExtra("notification_event", "onNotificationRemoved :" + sbn.getPackageName() + "\n");

        //sendBroadcast(i);
    }

    class NLServiceReceiver extends BroadcastReceiver
	{

        @Override
        public void onReceive(Context context, Intent intent)
		{
            if (intent.getStringExtra("command").equals("clearall"))
			{
				NRService.this.cancelAllNotifications();
            }
            else if (intent.getStringExtra("command").equals("list"))
			{
                Intent i1 = new  Intent("com.neoexpert.nreader.NOTIFICATION_LISTENER_SERVICE");
                i1.putExtra("notification_event", "=====================");
                sendBroadcast(i1);
                int i=1;
                for (StatusBarNotification sbn : NRService.this.getActiveNotifications())
				{
                    Intent i2 = new  Intent("com.neoexpert.nreader.NOTIFICATION_LISTENER");
                    i2.putExtra("notification_event", i + " " + sbn.getPackageName() + "\n");
                    sendBroadcast(i2);
                    i++;
                }
                Intent i3 = new  Intent("com.neoexpert.nreader.NOTIFICATION_LISTENER_SERVICE");
                i3.putExtra("notification_event", "===== Notification List ====");
                sendBroadcast(i3);

            }

        }
    }
	boolean isScreenOn(){
		DisplayManager dm = (DisplayManager) this.getSystemService(Context.DISPLAY_SERVICE);
		for (Display display : dm.getDisplays()) {
			switch (display.getState() ) {
				case Display.STATE_ON:
					return true;
			}
		}
		return false;
	}
	public class SettingsContentObserver extends ContentObserver {
		int previousVolume;
		Context context;

		public SettingsContentObserver(Context c, Handler handler) {
			super(handler);
			context=c;

			AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
			previousVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
		}

		@Override
		public boolean deliverSelfNotifications() {
			return super.deliverSelfNotifications();
		}

		@Override
		public void onChange(boolean selfChange) {
			super.onChange(selfChange);

			AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
			int currentVolume = audio.getStreamVolume(AudioManager.STREAM_VOICE_CALL);

			int delta=previousVolume-currentVolume;

			if(delta>0)
			{
				
				previousVolume=currentVolume;
			}
			else if(delta<0)
			{
				
				previousVolume=currentVolume;
			}
		}
	}
	
}
