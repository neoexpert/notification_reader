package com.neoexpert.nreader;


import android.app.*;
import android.content.*;
import android.content.pm.*;
import android.graphics.drawable.*;
import android.os.*;
import android.preference.*;
import android.support.design.widget.*;
import android.support.v4.app.*;
import android.view.*;
import android.view.View.*;
import android.widget.*;
import android.widget.CompoundButton.*;
import com.neoexpert.nreader.*;
import java.util.*;

import android.view.View.OnClickListener;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity 
implements
OnCheckedChangeListener,
OnClickListener
{
	static{
		
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.activitymainTest:
				NotificationManager nManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
				NotificationCompat.Builder ncomp = new NotificationCompat.Builder(this);
				ncomp.setContentTitle(getString(R.string.test_notification_title));
				ncomp.setContentText(getString(R.string.test_notification));
				ncomp.setTicker("");
				ncomp.setSmallIcon(R.drawable.ic_launcher);
				ncomp.setAutoCancel(true);
				nManager.notify((int)System.currentTimeMillis(), ncomp.build());

				break;

			case R.id.fab:
				Intent i=new Intent(this, PackageSelector.class);
				startActivityForResult(i, 2);
				break;


		}
	}


	@Override
	public void onCheckedChanged(CompoundButton v, boolean checked)
	{
		switch (v.getId())
		{
			case R.id.cbScreen:
				prefsEditor.putBoolean("readWhenScreenON", checked);
				prefsEditor.commit();
				break;
			case R.id.nrSwitch:
				if (!NRService.created && checked)
				{
					Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS);
					startActivityForResult(callGPSSettingIntent, 1);
				}
				prefsEditor.putBoolean("ServiceEnabled", checked);
				prefsEditor.commit();
				break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		switch (requestCode)
		{
			case 1:
				sw.setChecked(NRService.created);
				break;
			case 2:
				if (resultCode == 1)
				{
					String p=data.getExtras().getString("package");
					packgs.add(p);
					prefsEditor.putStringSet("packages", packgs);
					prefsEditor.commit();
					list.add(p);
					la.notifyDataSetChanged();
				}
		}
	}

    SharedPreferences prefs;
	SharedPreferences.Editor prefsEditor;
    private NotificationReceiver nReceiver;
	Set<String> packgs=new HashSet<String>();
	List<String> list;
	ListAdapter la;
	Switch sw;
    @Override
    protected void onCreate(Bundle savedInstanceState)
	{
		if (BuildConfig.DEBUG)
			Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
		
        super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		FloatingActionButton fab=(FloatingActionButton)findViewById(R.id.fab);
		fab.setOnClickListener(this);
		sw = (Switch)findViewById(R.id.nrSwitch);
		sw.setChecked(NRService.created);
		sw.setOnCheckedChangeListener(this);
		prefs = PreferenceManager.getDefaultSharedPreferences(this);
		prefsEditor = prefs.edit();
		prefsEditor.putBoolean("com.neoexpert.nreader",true);
		prefsEditor.commit();
		CheckBox cbs=(CheckBox)findViewById(R.id.cbScreen);
		cbs.setChecked(prefs.getBoolean("readWhenScreenON", false));
		cbs.setOnCheckedChangeListener(this);

        packgs = prefs.getStringSet("packages", packgs);

        nReceiver = new NotificationReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.neoexpert.nreader.NOTIFICATION_LISTENER_SERVICE");
        registerReceiver(nReceiver, filter);
		packgs.add("com.whatsapp");
		packgs.add("com.google.android.gm");
		packgs.add("com.facebook.orca");
		packgs.add("com.android.mms");
		packgs.add("org.thoughtcrime.securesms");
		packgs.add("com.skype.raider");
		packgs.add("com.snapchat.android");
		packgs.add("com.twitter.android");
		packgs.add("com.viber.voip");
		
		
		
		Iterator<String> it=packgs.iterator();
		while (it.hasNext())
			if (!isPackageInstalled(it.next(), this))
				it.remove();
		list = new ArrayList<String>(packgs);
		la = new ListAdapter(this, R.layout.listrow, list);
		ListView lv=(ListView)findViewById(R.id.pckg_list);
		lv.setAdapter(la);
	}
	private boolean isPackageInstalled(String packagename, Context context)
	{
		PackageManager pm = context.getPackageManager();
		try
		{
			pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
			return true;
		}
		catch (PackageManager.NameNotFoundException e)
		{
			return false;
		}
	}
    @Override
    protected void onDestroy()
	{
        super.onDestroy();
        unregisterReceiver(nReceiver);
    }





    class NotificationReceiver extends BroadcastReceiver
	{

        @Override
        public void onReceive(Context context, Intent intent)
		{
			switch (intent.getIntExtra("action", -1))
			{
				case 0:
					String pkg = intent.getStringExtra("package");
					list.add(pkg);
					la.notifyDataSetChanged();
					break;
				case 1:
					sw.setChecked(false);
			}

        }
    }

	public class ListAdapter extends ArrayAdapter<String>
	{

		public ListAdapter(Context context, int textViewResourceId)
		{
			super(context, textViewResourceId);
		}

		public ListAdapter(Context context, int resource, List<String> items)
		{
			super(context, resource, items);
		}
		private class CBHolder
		{
			protected String pkg;
			protected CheckBox cb;
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{

			View v = convertView;

			if (v == null)
			{
				LayoutInflater vi;
				vi = LayoutInflater.from(getContext());
				v = vi.inflate(R.layout.listrow, null);
			}

			String p = getItem(position);

			if (p != null)
			{
				TextView tt1 = (TextView) v.findViewById(R.id.listrowTV);
				ImageView iv=(ImageView)v.findViewById(R.id.listrowIV);

				final CBHolder cb=new CBHolder();
				cb.cb = (CheckBox)v.findViewById(R.id.listrowCB);
				cb.cb.setOnCheckedChangeListener(null);
				cb.cb.setChecked(prefs.getBoolean(getItem(position), false));
				cb.pkg = getItem(position);
				cb.cb.setOnCheckedChangeListener(new OnCheckedChangeListener(){

						@Override
						public void onCheckedChanged(CompoundButton p1, boolean checked)
						{
							prefsEditor.putBoolean(cb.pkg, checked);
							prefsEditor.commit();
							Toast.makeText(getContext(), cb.pkg, Toast.LENGTH_LONG).show();
						}
					});
				tt1.setText(p);
				try
				{
					Drawable icon = getPackageManager().getApplicationIcon(getItem(position));
					iv.setImageDrawable(icon);
				}
				catch (PackageManager.NameNotFoundException e)
				{}

			}

			return v;
		}

	}

}
